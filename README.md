<!----------------------------------------------------------------------------->

# dns (aws)

<!----------------------------------------------------------------------------->

## Description

Provides resources for managing Domain Name System (DNS) on [AWS] with
[Route 53].

<!----------------------------------------------------------------------------->

## Modules

* [route53/failover-record](route53/failover-record/README.md) - Manage [Route 53] failover DNS records within a hosted zone.
* [route53/record](route53/record/README.md) - Manage [Route 53] DNS records within a hosted zone.
* [route53/zone/public](route53/zone/public/README.md) - Manage public [Route 53] DNS zones.

<!----------------------------------------------------------------------------->

[AWS]:      https://aws.amazon.com/
[Route 53]: https://aws.amazon.com/route53/

<!----------------------------------------------------------------------------->
