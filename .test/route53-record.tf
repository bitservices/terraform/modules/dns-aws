################################################################################
# Modules
################################################################################

module "route53_record" {
  source    = "../route53/record"
  name      = format("foo.%s", module.route53_zone_public.name)
  zone_id   = module.route53_zone_public.id
  zone_name = module.route53_zone_public.name

  records = [
    "10.81.10.11",
    "10.81.10.12"
  ]
}

################################################################################
