################################################################################
# Modules
################################################################################

module "route53_failover_record" {
  source    = "../route53/failover-record"
  name      = format("foo.%s", module.route53_zone_public.name)
  owner     = local.owner
  company   = local.company
  zone_id   = module.route53_zone_public.id
  zone_name = module.route53_zone_public.name

  records_primary = [
    "10.81.10.11"
  ]

  records_secondary = [
    "10.81.10.12"
  ]
}

################################################################################
