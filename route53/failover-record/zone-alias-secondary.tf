################################################################################
# Optional Variables
################################################################################

variable "zone_alias_secondary" {
  type        = string
  default     = null
  description = "The hosted zone ID or name that contains the target for the alias record. Defaults to the same zone that the DNS record will be created in. Ignored unless 'alias' is 'true'."
}

variable "zone_alias_secondary_lookup" {
  type        = bool
  default     = false
  description = "Lookup hosted zone that contains the target for this alias record. Must be 'true' if 'zone_alias_secondary' is not an ID."
}

variable "zone_alias_secondary_private" {
  type        = bool
  default     = false
  description = "Is 'zone_alias_secondary' a private hosted zone. Ignored unless 'zone_alias_secondary_lookup' is 'true'."
}

################################################################################
# Locals
################################################################################

locals {
  zone_alias_secondary        = coalesce(var.zone_alias_secondary, data.aws_route53_zone.scope.zone_id)
  zone_alias_secondary_id     = length(data.aws_route53_zone.alias_secondary) == 1 ? data.aws_route53_zone.alias_secondary[var.zone_alias_secondary].zone_id : local.zone_alias_secondary
  zone_alias_secondary_lookup = var.alias_secondary && var.zone_alias_secondary_lookup
}

################################################################################
# Data Sources
################################################################################

data "aws_route53_zone" "alias_secondary" {
  for_each     = local.zone_alias_secondary_lookup ? toset([var.zone_alias_secondary]) : toset([])
  name         = local.zone_alias_secondary
  private_zone = var.zone_alias_secondary_private
}

################################################################################
# Outputs
################################################################################

output "zone_alias_secondary_lookup" {
  value = local.zone_alias_secondary_lookup
}

################################################################################

output "zone_alias_secondary_id" {
  value = local.zone_alias_secondary_id
}

################################################################################

output "zone_alias_secondary_ns" {
  value = length(data.aws_route53_zone.alias_secondary) == 1 ? data.aws_route53_zone.alias_secondary[var.zone_alias_secondary].name_servers : null
}

output "zone_alias_secondary_name" {
  value = length(data.aws_route53_zone.alias_secondary) == 1 ? data.aws_route53_zone.alias_secondary[var.zone_alias_secondary].name : null
}

output "zone_alias_secondary_private" {
  value = length(data.aws_route53_zone.alias_secondary) == 1 ? data.aws_route53_zone.alias_secondary[var.zone_alias_secondary].private_zone : null
}

################################################################################

