################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of the Route 53 record."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################

variable "records_primary" {
  type        = list(string)
  description = "A list of DNS entries for the primary DNS record. If 'alias_primary' is 'true' only the first value in the list is used."

  validation {
    condition     = var.records_primary != null && length(var.records_primary) > 0
    error_message = "The list of records to create for the primary DNS record must be a list with at least one value."
  }
}

variable "records_secondary" {
  type        = list(string)
  description = "A list of DNS entries for the failover DNS record. If 'alias_secondary' is 'true' only the first value in the list is used."

  validation {
    condition     = var.records_secondary != null && length(var.records_secondary) > 0
    error_message = "The list of records to create for the failover DNS record must be a list with at least one value."
  }
}

################################################################################
# Optional Variables
################################################################################

variable "ttl" {
  type        = number
  default     = 300
  description = "The TTL of the record in seconds. Ignored for the primary DNS record if 'alias_primary' is 'true' and ignored for the failover DNS record if 'alias_secondary' is 'true'."
}

variable "type" {
  type        = string
  default     = "A"
  description = "The record type."
}

################################################################################

variable "alias_primary" {
  type        = bool
  default     = false
  description = "Specifies whether or not the primary DNS record is an alias record."
}

variable "alias_secondary" {
  type        = bool
  default     = false
  description = "Specifies whether or not the failover DNS record is an alias record."
}

variable "alias_health_primary" {
  type        = bool
  default     = false
  description = "Evaluate target health for the primary alias record? Ignored unless 'alias_primary' is 'true'."
}

variable "alias_health_secondary" {
  type        = bool
  default     = false
  description = "Evaluate target health for the secondary alias record? Ignored unless 'alias_secondary' is 'true'."
}

################################################################################

variable "failover_type_primary" {
  type        = string
  default     = "PRIMARY"
  description = "Unique identifier and routing policy type for the 'primary' record in the failover pair."
}

variable "failover_type_secondary" {
  type        = string
  default     = "SECONDARY"
  description = "Unique identifier and routing policy type for the 'secondary' record in the failover pair."
}

################################################################################
# Locals
################################################################################

locals {
  ttl_primary            = var.alias_primary ? null : var.ttl
  ttl_secondary          = var.alias_secondary ? null : var.ttl
  records_primary        = var.alias_primary ? null : var.records_primary
  records_secondary      = var.alias_secondary ? null : var.records_secondary
  identifier_primary     = format("%s_%s", var.name, var.failover_type_primary)
  identifier_secondary   = format("%s_%s", var.name, var.failover_type_secondary)
  alias_health_primary   = var.alias_primary && var.alias_health_primary
  alias_record_primary   = var.alias_primary ? var.records_primary[0] : null
  alias_health_secondary = var.alias_secondary && var.alias_health_secondary
  alias_record_secondary = var.alias_secondary ? var.records_secondary[0] : null
}

################################################################################
# Resources
################################################################################

resource "aws_route53_record" "primary" {
  ttl             = local.ttl_primary
  type            = var.type
  name            = var.name
  records         = local.records_primary
  zone_id         = data.aws_route53_zone.scope.zone_id
  set_identifier  = local.identifier_primary
  health_check_id = aws_route53_health_check.healthcheck.id

  dynamic "alias" {
    for_each = var.alias_primary ? [null] : []

    content {
      name                   = local.alias_record_primary
      zone_id                = local.zone_alias_primary_id
      evaluate_target_health = local.alias_health_primary
    }
  }

  failover_routing_policy {
    type = var.failover_type_primary
  }
}

resource "aws_route53_record" "secondary" {
  ttl            = local.ttl_secondary
  type           = var.type
  name           = var.name
  records        = local.records_secondary
  zone_id        = data.aws_route53_zone.scope.zone_id
  set_identifier = local.identifier_secondary

  dynamic "alias" {
    for_each = var.alias_secondary ? [null] : []

    content {
      name                   = local.alias_record_secondary
      zone_id                = local.zone_alias_secondary_id
      evaluate_target_health = local.alias_health_secondary
    }
  }

  failover_routing_policy {
    type = var.failover_type_secondary
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "alias_primary" {
  value = var.alias_primary
}

output "alias_secondary" {
  value = var.alias_secondary
}

output "alias_health_primary" {
  value = local.alias_health_primary
}

output "alias_health_secondary" {
  value = local.alias_health_secondary
}

################################################################################

output "failover_type_primary" {
  value = var.failover_type_primary
}

output "failover_type_secondary" {
  value = var.failover_type_secondary
}

################################################################################

output "identifier_primary" {
  value = local.identifier_primary
}

output "identifier_secondary" {
  value = local.identifier_secondary
}

################################################################################

output "ttl_primary" {
  value = aws_route53_record.primary.ttl
}

output "fqdn_primary" {
  value = aws_route53_record.primary.fqdn
}

output "name_primary" {
  value = aws_route53_record.primary.name
}

output "type_primary" {
  value = aws_route53_record.primary.type
}

output "records_primary" {
  value = var.alias_primary ? [for alias in aws_route53_record.primary.alias : alias.name] : aws_route53_record.primary.records
}

################################################################################

output "ttl_secondary" {
  value = aws_route53_record.secondary.ttl
}

output "fqdn_secondary" {
  value = aws_route53_record.secondary.fqdn
}

output "name_secondary" {
  value = aws_route53_record.secondary.name
}

output "type_secondary" {
  value = aws_route53_record.secondary.type
}

output "records_secondary" {
  value = var.alias_secondary ? [for alias in aws_route53_record.secondary.alias : alias.name] : aws_route53_record.secondary.records
}

################################################################################
