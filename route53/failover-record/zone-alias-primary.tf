################################################################################
# Optional Variables
################################################################################

variable "zone_alias_primary" {
  type        = string
  default     = null
  description = "The hosted zone ID or name that contains the target for the alias record. Defaults to the same zone that the DNS record will be created in. Ignored unless 'alias' is 'true'."
}

variable "zone_alias_primary_lookup" {
  type        = bool
  default     = false
  description = "Lookup hosted zone that contains the target for this alias record. Must be 'true' if 'zone_alias_primary' is not an ID."
}

variable "zone_alias_primary_private" {
  type        = bool
  default     = false
  description = "Is 'zone_alias_primary' a private hosted zone. Ignored unless 'zone_alias_primary_lookup' is 'true'."
}

################################################################################
# Locals
################################################################################

locals {
  zone_alias_primary        = coalesce(var.zone_alias_primary, data.aws_route53_zone.scope.zone_id)
  zone_alias_primary_id     = length(data.aws_route53_zone.alias_primary) == 1 ? data.aws_route53_zone.alias_primary[var.zone_alias_primary].zone_id : local.zone_alias_primary
  zone_alias_primary_lookup = var.alias_primary && var.zone_alias_primary_lookup
}

################################################################################
# Data Sources
################################################################################

data "aws_route53_zone" "alias_primary" {
  for_each     = local.zone_alias_primary_lookup ? toset([var.zone_alias_primary]) : toset([])
  name         = local.zone_alias_primary
  private_zone = var.zone_alias_primary_private
}

################################################################################
# Outputs
################################################################################

output "zone_alias_primary_lookup" {
  value = local.zone_alias_primary_lookup
}

################################################################################

output "zone_alias_primary_id" {
  value = local.zone_alias_primary_id
}

################################################################################

output "zone_alias_primary_ns" {
  value = length(data.aws_route53_zone.alias_primary) == 1 ? data.aws_route53_zone.alias_primary[var.zone_alias_primary].name_servers : null
}

output "zone_alias_primary_name" {
  value = length(data.aws_route53_zone.alias_primary) == 1 ? data.aws_route53_zone.alias_primary[var.zone_alias_primary].name : null
}

output "zone_alias_primary_private" {
  value = length(data.aws_route53_zone.alias_primary) == 1 ? data.aws_route53_zone.alias_primary[var.zone_alias_primary].private_zone : null
}

################################################################################

