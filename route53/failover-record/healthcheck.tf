################################################################################
# Optional Variables
################################################################################

variable "healthcheck_host" {
  type        = string
  default     = null
  description = "The endpoint for the healthcheck to test. Defaults to the first primary record."
}

variable "healthcheck_name" {
  type        = string
  default     = null
  description = "This is a reference name used in caller reference."
}

variable "healthcheck_type" {
  type        = string
  default     = "TCP"
  description = "The protocol to use when performing health checks. Valid values are 'HTTP', 'HTTPS', 'HTTP_STR_MATCH', 'HTTPS_STR_MATCH', 'TCP', 'CALCULATED' and 'CLOUDWATCH_METRIC'."
}

variable "healthcheck_port" {
  type        = number
  default     = 443
  description = "The port of the endpoint to be checked."
}

variable "healthcheck_resource_path" {
  type        = string
  default     = "/"
  description = "The path that you want Amazon Route 53 to request when performing health checks. Applies if 'healthcheck_type' is 'HTTP', 'HTTPS', 'HTTP_STR_MATCH' or 'HTTPS_STR_MATCH'."
}

variable "healthcheck_request_interval" {
  type        = number
  default     = 30
  description = "The number of seconds between the time that Amazon Route 53 gets a response from your endpoint and the time that it sends the next health-check request."
}

variable "healthcheck_failure_threshold" {
  type        = number
  default     = 3
  description = "The number of consecutive health checks that an endpoint must pass or fail."
}

################################################################################
# Locals
################################################################################

locals {
  healthcheck_host          = coalesce(var.healthcheck_host, var.records_primary[0])
  healthcheck_name          = coalesce(var.healthcheck_name, random_string.autoname.result)
  healthcheck_resource_path = contains(["HTTP", "HTTPS", "HTTP_STR_MATCH", "HTTPS_STR_MATCH"], var.healthcheck_type) ? var.healthcheck_resource_path : null
}

################################################################################
# Resources
################################################################################

resource "random_string" "autoname" {
  length  = 16
  special = false
}

################################################################################

resource "aws_route53_health_check" "healthcheck" {
  fqdn              = local.healthcheck_host
  port              = var.healthcheck_port
  type              = var.healthcheck_type
  resource_path     = local.healthcheck_resource_path
  reference_name    = local.healthcheck_name
  request_interval  = var.healthcheck_request_interval
  failure_threshold = var.healthcheck_failure_threshold

  tags = {
    Name       = local.healthcheck_name
    Owner      = var.owner
    Record     = var.name
    Company    = var.company
    HostedZone = data.aws_route53_zone.scope.zone_id
  }
}

################################################################################
# Outputs
################################################################################

output "healthcheck_host" {
  value = local.healthcheck_host
}

output "healthcheck_name" {
  value = local.healthcheck_name
}

output "healthcheck_type" {
  value = var.healthcheck_type
}

output "healthcheck_port" {
  value = var.healthcheck_port
}

output "healthcheck_resource_path" {
  value = local.healthcheck_resource_path
}

output "healthcheck_request_interval" {
  value = var.healthcheck_request_interval
}

output "healthcheck_failure_threshold" {
  value = var.healthcheck_failure_threshold
}

################################################################################

output "healthcheck_id" {
  value = aws_route53_health_check.healthcheck.id
}

################################################################################
