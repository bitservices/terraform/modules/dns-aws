<!----------------------------------------------------------------------------->

# route53/failover-record

#### Manage [Route 53] failover DNS records within a hosted zone

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/aws//route53/failover-record`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "my_dns_base" { default = "bitservices.io" }
variable "my_dns_name" { default = "foobar"         }

locals {
  record_name = format("%s.%s", var.my_dns_name, var.my_dns_base)
}

module "my_failover_record" {
  source    = "gitlab.com/bitservices/database/aws//route53/failover-record"
  name      = local.record_name
  owner     = "terraform@bitservices.io"
  company   = "BITServices Ltd"
  zone_name = var.my_dns_base

  records_primary = [
    "10.81.10.11"
  ]

  records_secondary = [
    "10.81.11.11"
  ]
}
```

<!----------------------------------------------------------------------------->

[Route 53]: https://aws.amazon.com/route53/

<!----------------------------------------------------------------------------->
