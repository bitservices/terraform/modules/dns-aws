<!----------------------------------------------------------------------------->

# route53/zone/public

#### Manage public [Route 53] DNS zones

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/aws//route53/zone/public`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_public_zone" {
  source  = "gitlab.com/bitservices/database/aws//route53/zone/public"
  name    = "foobar.bitservices.io"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[Route 53]: https://aws.amazon.com/route53/

<!----------------------------------------------------------------------------->
