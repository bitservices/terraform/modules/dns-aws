################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of the Route 53 public zone."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "type" {
  type        = string
  default     = "public"
  description = "Used to set the Route 53 zones 'Type' tag. Should always be 'public'."
}

variable "auto_destroy" {
  type        = bool
  default     = true
  description = "Whether to automatically destroy all records (possibly managed outside of Terraform) in this public Route 53 zone."
}

################################################################################
# Resources
################################################################################

resource "aws_route53_zone" "scope" {
  name          = var.name
  force_destroy = var.auto_destroy

  tags = {
    Name    = var.name
    Type    = var.type
    Owner   = var.owner
    Region  = data.aws_region.scope.name
    Company = var.company
  }
}

################################################################################
# Outputs
################################################################################

output "type" {
  value = var.type
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "id" {
  value = aws_route53_zone.scope.zone_id
}

output "name" {
  value = aws_route53_zone.scope.name
}

output "auto_destroy" {
  value = aws_route53_zone.scope.force_destroy
}

output "name_servers" {
  value = aws_route53_zone.scope.name_servers
}

################################################################################
