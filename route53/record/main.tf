################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of the Route 53 record."
}

################################################################################

variable "records" {
  type        = list(string)
  description = "A string list of DNS entries. If 'alias' is 'true' only the first value in the list is used."

  validation {
    condition     = var.records != null && length(var.records) > 0
    error_message = "The list of records to create must be a list with at least one value."
  }
}

################################################################################
# Optional Variables
################################################################################

variable "ttl" {
  type        = number
  default     = 300
  description = "The TTL of the record in seconds. Ignored if 'alias' is 'true'."
}

variable "type" {
  type        = string
  default     = "A"
  description = "The record type."
}

################################################################################

variable "alias" {
  type        = bool
  default     = false
  description = "Specifies whether or not this is an alias record."
}

variable "alias_health" {
  type        = bool
  default     = false
  description = "Evaluate target health for alias record? Ignored unless 'alias' is 'true'."
}

################################################################################
# Locals
################################################################################

locals {
  ttl          = var.alias ? null : var.ttl
  records      = var.alias ? null : var.records
  alias_health = var.alias && var.alias_health
  alias_record = var.alias ? var.records[0] : null
}

################################################################################
# Resources
################################################################################

resource "aws_route53_record" "scope" {
  ttl     = local.ttl
  type    = var.type
  name    = var.name
  records = local.records
  zone_id = data.aws_route53_zone.scope.zone_id

  dynamic "alias" {
    for_each = var.alias ? [null] : []

    content {
      name                   = local.alias_record
      zone_id                = local.zone_alias_id
      evaluate_target_health = local.alias_health
    }
  }
}

################################################################################
# Outputs
################################################################################

output "alias" {
  value = var.alias
}

output "alias_health" {
  value = local.alias_health
}

################################################################################

output "ttl" {
  value = aws_route53_record.scope.ttl
}

output "fqdn" {
  value = aws_route53_record.scope.fqdn
}

output "name" {
  value = aws_route53_record.scope.name
}

output "type" {
  value = aws_route53_record.scope.type
}

output "records" {
  value = var.alias ? [for alias in aws_route53_record.scope.alias : alias.name] : aws_route53_record.scope.records
}

################################################################################
