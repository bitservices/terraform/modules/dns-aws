<!----------------------------------------------------------------------------->

# route53/record

#### Manage [Route 53] DNS records within a hosted zone

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/database/aws//route53/record`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "my_dns_base" { default = "bitservices.io" }
variable "my_dns_name" { default = "foobar"         }

locals {
  record_name = format("%s.%s", var.my_dns_name, var.my_dns_base)
}

module "my_record" {
  source    = "gitlab.com/bitservices/database/aws//route53/record"
  name      = local.record_name
  zone_name = var.my_dns_base

  records = [
    "10.81.10.11",
    "10.81.10.12"
  ]
}
```

<!----------------------------------------------------------------------------->

[Route 53]: https://aws.amazon.com/route53/

<!----------------------------------------------------------------------------->
