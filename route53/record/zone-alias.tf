################################################################################
# Optional Variables
################################################################################

variable "zone_alias" {
  type        = string
  default     = null
  description = "The hosted zone ID or name that contains the target for the alias record. Defaults to the same zone that the DNS record will be created in. Ignored unless 'alias' is 'true'."
}

variable "zone_alias_lookup" {
  type        = bool
  default     = false
  description = "Lookup hosted zone that contains the target for this alias record. Must be 'true' if 'zone_alias' is not an ID."
}

variable "zone_alias_private" {
  type        = bool
  default     = false
  description = "Is 'zone_alias' a private hosted zone. Ignored unless 'zone_alias_lookup' is 'true'."
}

################################################################################
# Locals
################################################################################

locals {
  zone_alias        = coalesce(var.zone_alias, data.aws_route53_zone.scope.zone_id)
  zone_alias_id     = length(data.aws_route53_zone.alias) == 1 ? data.aws_route53_zone.alias[var.zone_alias].zone_id : local.zone_alias
  zone_alias_lookup = var.alias && var.zone_alias_lookup
}

################################################################################
# Data Sources
################################################################################

data "aws_route53_zone" "alias" {
  for_each     = local.zone_alias_lookup ? toset([var.zone_alias]) : toset([])
  name         = local.zone_alias
  private_zone = var.zone_alias_private
}

################################################################################
# Outputs
################################################################################

output "zone_alias_lookup" {
  value = local.zone_alias_lookup
}

################################################################################

output "zone_alias_id" {
  value = local.zone_alias_id
}

################################################################################

output "zone_alias_ns" {
  value = length(data.aws_route53_zone.alias) == 1 ? data.aws_route53_zone.alias[var.zone_alias].name_servers : null
}

output "zone_alias_name" {
  value = length(data.aws_route53_zone.alias) == 1 ? data.aws_route53_zone.alias[var.zone_alias].name : null
}

output "zone_alias_private" {
  value = length(data.aws_route53_zone.alias) == 1 ? data.aws_route53_zone.alias[var.zone_alias].private_zone : null
}

################################################################################

