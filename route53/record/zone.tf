################################################################################
# Required Variables
################################################################################

variable "zone_name" {
  type        = string
  description = "The hosted zone name to contain the new record."
}

################################################################################
# Optional Variables
################################################################################

variable "zone_id" {
  type        = string
  default     = null
  description = "The hosted zone ID. Must be specified if hosted zone is created in the same Terraform run."
}

variable "zone_private" {
  type        = bool
  default     = false
  description = "Is 'zone_name' a private hosted zone."
}

################################################################################
# Locals
################################################################################

locals {
  zone_private = var.zone_id != null ? var.zone_private : null
}

################################################################################
# Data Sources
################################################################################

data "aws_route53_zone" "scope" {
  name         = var.zone_name
  zone_id      = var.zone_id
  private_zone = local.zone_private
}

################################################################################
# Outputs
################################################################################

output "zone_id" {
  value = data.aws_route53_zone.scope.zone_id
}

output "zone_ns" {
  value = data.aws_route53_zone.scope.name_servers
}

output "zone_name" {
  value = data.aws_route53_zone.scope.name
}

output "zone_private" {
  value = data.aws_route53_zone.scope.private_zone
}

################################################################################
